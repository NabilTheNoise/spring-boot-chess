package com.games.online.controllers.chess;

import com.games.online.models.User;
import com.games.online.models.chess.EGameStatus;
import com.games.online.models.chess.Game;
import com.games.online.models.chess.GameStatus;
import com.games.online.models.chess.Player;
import com.games.online.payloads.requests.CreateGameRequest;
import com.games.online.payloads.requests.MovePieceRequest;
import com.games.online.payloads.response.*;
import com.games.online.repositories.UserRepository;
import com.games.online.repositories.chess.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/game")
public class GameController {
    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private UserRepository userRepository;


    @PostMapping("/create")
    public ResponseEntity<?> createGame(Authentication authentication, @Valid @RequestBody CreateGameRequest createGameRequest) {
        String username = authentication.getName();

        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("Username %s not found!", username)));

        Player player = new Player(user, createGameRequest.isWhiteSide());

        Game game = new Game(player);

        gameRepository.save(game);

        return ResponseEntity.ok(new CreateGameResponse(game.getId()));
    }


    @GetMapping("/{gameId}")
    public ResponseEntity<?> getGameInformation(@PathVariable String gameId) {
        Game game = gameRepository.findById(gameId)
                .orElseThrow(() -> new RuntimeException(String.format("Game id %s not found.", gameId)));

        return ResponseEntity.ok(new GameInfoResponse(game));
    }


    @GetMapping("/{gameId}/join")
    public ResponseEntity<?> joinGame(Authentication authentication, @PathVariable("gameId") String gameId) {
        Game game = gameRepository.findById(gameId)
                .orElseThrow(() -> new RuntimeException(String.format("Game id %s not found.", gameId)));

        String username = authentication.getName();
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("Username %s not found!", username)));

        Player[] players = game.getPlayers();
        if (players[0].getUser().getUsername().equals(user.getUsername())) {
            return ResponseEntity
                    .ok(new MessageResponse("User already in the game"));
        }

        if (players[1] != null) {
            if (players[1].getUser().getUsername().equals(user.getUsername())) {
                return ResponseEntity
                        .ok(new MessageResponse("User already in the game"));
            }
        }

        if (players[1] != null) {
            return ResponseEntity.badRequest()
                    .body(new MessageResponse("Game is full"));
        }

        boolean isWhiteSide = !players[0].isWhiteSide();

        Player player = new Player(user, isWhiteSide);

        players[1] = player;

        game.setPlayers(players);

        if (game.getCurrentTurn() == null && player.isWhiteSide()) {
            game.setCurrentTurn(player);
        }

        game.setGameStatus(new GameStatus(EGameStatus.ACTIVE));

        gameRepository.save(game);

        return ResponseEntity.ok(
                new JoinGameResponse("Joined successfully", game.getId(), isWhiteSide)
        );
    }


    @PostMapping("/{gameId}")
    public ResponseEntity<?> movePieces(
            Authentication authentication,
            @PathVariable String gameId,
            @Valid @RequestBody MovePieceRequest move) throws Exception {

        Game game = gameRepository.findById(gameId)
                .orElseThrow(() -> new RuntimeException(String.format("Game id %s not found.", gameId)));

        String username = authentication.getName();
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("Username %s not found!", username)));

        if (game.getGameStatus().getName().equals(EGameStatus.ACTIVE)) {
            if (game.getCurrentTurn().getPlayerUsername().equals(user.getUsername())) {
                boolean isValidMove = game.playerMove(
                        game.getCurrentTurn(),
                        move.getStartX(),
                        move.getStartY(),
                        move.getDestX(),
                        move.getDestY(),
                        move.getConvertPawnTo()
                );

                if (!game.getGameStatus().getName().equals(EGameStatus.ACTIVE)) {
                    gameRepository.save(game);
                    return ResponseEntity.ok(new MessageResponse(String.format("Game Status: %s", game.getGameStatus().getName())));
                }

                if (!isValidMove) {
                    return ResponseEntity.ok(new MessageResponse("Move is not valid!"));
                }

                gameRepository.save(game);

                return ResponseEntity.ok(new GameInfoResponse(game));

            } else {
                return ResponseEntity.badRequest().body(new MessageResponse("It's not your turn."));
            }
        } else {
            return ResponseEntity.ok(new MessageResponse(String.format("Game has finished. Status: %s", game.getGameStatus().getName())));
        }
    }


    @DeleteMapping("/{gameId}")
    public ResponseEntity<?> deleteGame(Authentication authentication, @PathVariable("gameId") String gameId) {
        Game game = gameRepository.findById(gameId)
                .orElseThrow(() -> new RuntimeException(String.format("Game with id %s not found.", gameId)));

        if (game.getPlayers()[0].getUser().getUsername().equals(authentication.getName())) {
            gameRepository.deleteById(gameId);
            return ResponseEntity.ok(new MessageResponse(String.format("Game with id %s removed successfully", gameId)));
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new MessageResponse("Access Denied!"));
        }
    }


    @GetMapping
    public ResponseEntity<?> getUserGames(Authentication authentication) {
        User user = userRepository.findByUsername(authentication.getName())
                .orElseThrow();
        Player player = new Player(
                user,
                false
        );
        List<SimpleGameInfoResponse> gameInfoResponses = new ArrayList<>();
        for (Game games : gameRepository.findAllByPlayers(player)) {
            gameInfoResponses.add(new SimpleGameInfoResponse(games, player));
        }
        player.setWhiteSide(true);
        for (Game games : gameRepository.findAllByPlayers(player)) {
            gameInfoResponses.add(new SimpleGameInfoResponse(games, player));
        }
        return ResponseEntity.ok(gameInfoResponses);
    }
}
