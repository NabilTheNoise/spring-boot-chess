package com.games.online.controllers.auth;

import com.games.online.models.ERole;
import com.games.online.models.Role;
import com.games.online.models.User;
import com.games.online.payloads.requests.ChangePasswordRequest;
import com.games.online.payloads.requests.LoginRequest;
import com.games.online.payloads.requests.SignupRequest;
import com.games.online.payloads.response.LoginResponse;
import com.games.online.payloads.response.MessageResponse;
import com.games.online.repositories.RoleRepository;
import com.games.online.repositories.UserRepository;
import com.games.online.security.jwt.JwtUtils;
import com.games.online.security.services.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signupRequest) {
        if (userRepository.existsByUsername(signupRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new RuntimeException("Username already exists."));

        }

        if (userRepository.existsByEmail(signupRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new RuntimeException("Email already exists."));

        }

        User user = new User(
                signupRequest.getUsername(),
                signupRequest.getEmail(),
                passwordEncoder.encode(signupRequest.getPassword())
        );

        if (!roleRepository.existsByName(ERole.ROLE_USER)) {
            roleRepository.save(new Role(ERole.ROLE_USER));
        }
        if (!roleRepository.existsByName(ERole.ROLE_ADMIN)) {
            roleRepository.save(new Role(ERole.ROLE_ADMIN));
        }
        if (!roleRepository.existsByName(ERole.ROLE_MODERATOR)) {
            roleRepository.save(new Role(ERole.ROLE_MODERATOR));
        }

        Set<String> requestRoles = signupRequest.getRoles();
        Set<Role> roles = new HashSet<>();

        if (requestRoles == null) {
            roles.add(
                    roleRepository.findByName(ERole.ROLE_USER).orElseThrow(
                            () -> new RuntimeException("User role not found!")
                    )
            );
        } else {
            requestRoles.forEach(role -> {
                if (role.equals("admin")) {
                    roles.add(
                            roleRepository.findByName(ERole.ROLE_ADMIN).orElseThrow(
                                    () -> new RuntimeException("User role not found!")
                            )
                    );
                } else if (role.equals("mod")) {
                    roles.add(
                            roleRepository.findByName(ERole.ROLE_MODERATOR).orElseThrow(
                                    () -> new RuntimeException("Moderator role not found!")
                            )
                    );
                } else {
                    roles.add(
                            roleRepository.findByName(ERole.ROLE_USER).orElseThrow(
                                    () -> new RuntimeException("User role not found!")
                            )
                    );
                }
            });
        }

        user.setRoles(roles);
        userRepository.save(user);

        return ResponseEntity
                .ok()
                .body(new MessageResponse("User created successfully"));

    }

    @PostMapping("/login")
    public ResponseEntity<?> loginUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwtToken = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        List<String> roles = userDetails.getAuthorities().stream()
                .map(role -> role.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(
                new LoginResponse(
                        jwtToken,
                        userDetails.getId(),
                        userDetails.getUsername(),
                        userDetails.getEmail(),
                        roles
                )
        );
    }

    @GetMapping("/my-profile")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN') or hasRole('MODERATOR')")
    public User userProfile(Authentication authentication) {
        return userRepository.findByUsername(authentication.getName())
                .orElseThrow(() -> new UsernameNotFoundException(String.format("Username %s not found.", authentication.getName())));
    }

    @PostMapping("/change-password")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN') or hasRole('MODERATOR')")
    public ResponseEntity<?> changeUserPassword(
            Authentication authentication, @Valid @RequestBody ChangePasswordRequest changePasswordRequest) {
        User user = userRepository.findByUsername(authentication.getName())
                .orElseThrow(() -> new UsernameNotFoundException(String.format("Username %s not found.", authentication.getName())));

        user.setPassword(passwordEncoder.encode(changePasswordRequest.getNewPassword()));
        userRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("Password changed successfully"));
    }
}
