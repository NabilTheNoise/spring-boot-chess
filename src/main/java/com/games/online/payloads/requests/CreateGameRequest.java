package com.games.online.payloads.requests;

public class CreateGameRequest {
    private boolean whiteSide = true;

    public boolean isWhiteSide() {
        return whiteSide;
    }

    public void setWhiteSide(boolean whiteSide) {
        this.whiteSide = whiteSide;
    }
}
