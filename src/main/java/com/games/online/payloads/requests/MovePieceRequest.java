package com.games.online.payloads.requests;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class MovePieceRequest {
    @NotBlank(message = "from field should not be blank")
    @Size(min = 2, max = 2, message = "from field should be 2 characters")
    @Pattern(regexp = "[A-Ha-h1-8][1-8]", message = "from field first character should be A-Ha-h1-8 and the second character should be 1-8")
    private String from;
    @NotBlank(message = "to field should not be blank")
    @Size(min = 2, max = 2, message = "to field should be 2 characters")
    @Pattern(regexp = "[A-Ha-h1-8][1-8]", message = "to field first character should be A-Ha-h1-8 and the second character should be 1-8")
    private String to;

    private String convertPawnTo = "";

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getConvertPawnTo() {
        return convertPawnTo;
    }

    public void setConvertPawnTo(String convertPawnTo) {
        this.convertPawnTo = convertPawnTo;
    }

    public Integer getStartX(){
        String part = this.from.split("")[0];
        switch (part){
            case "A":
            case "a":
            case "1":
                return 7;

            case "B":
            case "b":
            case "2":
                return 6;

            case "C":
            case "c":
            case "3":
                return 5;

            case "D":
            case "d":
            case "4":
                return 4;

            case "E":
            case "e":
            case "5":
                return 3;

            case "F":
            case "f":
            case "6":
                return 2;

            case "G":
            case "g":
            case "7":
                return 1;

            case "H":
            case "h":
            case "8":
                return 0;
        }
        return null;
    }

    public Integer getStartY() {
        String part = this.from.split("")[1];
        return Integer.parseInt(part) - 1;
    }

    public Integer getDestX(){
        String part = this.to.split("")[0];
        switch (part){
            case "A":
            case "a":
            case "1":
                return 7;

            case "B":
            case "b":
            case "2":
                return 6;

            case "C":
            case "c":
            case "3":
                return 5;

            case "D":
            case "d":
            case "4":
                return 4;

            case "E":
            case "e":
            case "5":
                return 3;

            case "F":
            case "f":
            case "6":
                return 2;

            case "G":
            case "g":
            case "7":
                return 1;

            case "H":
            case "h":
            case "8":
                return 0;
        }
        return null;
    }

    public Integer getDestY() {
        String part = this.to.split("")[1];
        return Integer.parseInt(part) - 1;
    }
}
