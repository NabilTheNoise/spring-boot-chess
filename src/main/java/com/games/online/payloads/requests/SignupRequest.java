package com.games.online.payloads.requests;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

public class SignupRequest {
    @NotBlank(message = "Username is required.")
    @Size(min = 3, max = 50)
    private String username;

    @NotBlank(message = "Email is required.")
    @Size(min = 3, max = 50, message = "Email should be between 3 and 50 characters.")
    @Email(message = "Email should be valid!")
    private String email;

    @NotBlank(message = "Password is required.")
    @Size(min = 4, max = 120, message = "Password should be between 4 and 120 characters.")
    private String password;

    private Set<String> roles;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<String> getRoles() {
        return this.roles;
    }

    public void setRole(Set<String> roles) {
        this.roles = roles;
    }

}
