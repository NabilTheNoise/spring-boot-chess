package com.games.online.payloads.requests;

public class CreateTestGameRequest {
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
