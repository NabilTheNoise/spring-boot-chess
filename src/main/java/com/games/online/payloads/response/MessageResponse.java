package com.games.online.payloads.response;

import java.util.Date;

public class MessageResponse {
    private Date date;
    private String message;

    public MessageResponse(String message) {
        this.message = message;
        date = new Date();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
