package com.games.online.payloads.response;

public class CreateGameResponse {
    private String gameId;
    private String invitationLink;

    public CreateGameResponse(String gameId) {
        this.gameId = gameId;
        this.invitationLink = String.format("/api/v1/game/%s/join", gameId);
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getInvitationLink() {
        return invitationLink;
    }

    public void setInvitationLink(String invitationLink) {
        this.invitationLink = invitationLink;
    }
}
