package com.games.online.payloads.response;

public class JoinGameResponse {
    private String message;
    private String gameId;
    private boolean whiteSide;

    public JoinGameResponse(String message, String gameId, boolean whiteSide) {
        this.message = message;
        this.gameId = gameId;
        this.whiteSide = whiteSide;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public boolean isWhiteSide() {
        return whiteSide;
    }

    public void setWhiteSide(boolean whiteSide) {
        this.whiteSide = whiteSide;
    }
}
