package com.games.online.payloads.response;

import com.games.online.models.chess.*;

public class GameInfoResponse {
    private String gameId;
    private String[][] board = new String[8][8];
    private Player[] players;
    private Player currentTurn;
    private GameStatus gameStatus;

    public GameInfoResponse(Game game) {
        this.gameId = game.getId();
        this.players = game.getPlayers();
        this.currentTurn = game.getCurrentTurn();
        this.gameStatus = game.getGameStatus();
        createBoard(game.getBoard());
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public Player[] getPlayers() {
        return players;
    }

    public void setPlayers(Player[] players) {
        this.players = players;
    }

    public Player getCurrentTurn() {
        return currentTurn;
    }

    public void setCurrentTurn(Player currentTurn) {
        this.currentTurn = currentTurn;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    public void createBoard(Board board) {
        Spot spot;
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                try {
                    spot = board.getBox(x, y);
                    if (spot.getPiece() == null) {
                        this.board[y][x] = "";
                    } else {
                        this.board[y][x] = spot.getPiece().getName();
                    }
                } catch (Exception ignored) {
                }
            }
        }
    }

    public String[][] getBoard() {
        return board;
    }

    public void setBoard(String[][] board) {
        this.board = board;
    }
}
