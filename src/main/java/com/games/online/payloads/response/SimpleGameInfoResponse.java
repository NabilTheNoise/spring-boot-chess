package com.games.online.payloads.response;

import com.games.online.models.chess.EGameStatus;
import com.games.online.models.chess.Game;
import com.games.online.models.chess.GameStatus;
import com.games.online.models.chess.Player;

public class SimpleGameInfoResponse {
    private String id;
    private boolean isWhite;
    private EGameStatus status;
    private String player1;
    private String player2;

    public SimpleGameInfoResponse(Game game, Player player) {
        this.id = game.getId();
        this.player1 = game.getPlayers()[0].getPlayerUsername();
        if (game.getPlayers()[1] != null) {
            this.player2 = game.getPlayers()[1].getPlayerUsername();
        } else {
            this.player2 = "";
        }
        this.isWhite = player.isWhiteSide();
        this.status = game.getGameStatus().getName();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isWhite() {
        return isWhite;
    }

    public void setWhite(boolean white) {
        isWhite = white;
    }

    public EGameStatus getStatus() {
        return status;
    }

    public void setStatus(EGameStatus status) {
        this.status = status;
    }

    public String getPlayer1() {
        return player1;
    }

    public void setPlayer1(String player1) {
        this.player1 = player1;
    }

    public String getPlayer2() {
        return player2;
    }

    public void setPlayer2(String player2) {
        this.player2 = player2;
    }
}
