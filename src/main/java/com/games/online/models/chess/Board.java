package com.games.online.models.chess;

import com.games.online.models.chess.pieces.*;

public class Board {
    Spot[][] boxes;


    public Board() {
        this.boxes = new Spot[8][8];
        this.resetBoard();
    }

    public Board(String type) {
        this.boxes = new Spot[8][8];
        switch (type) {
            case "pawn":
                testPiece(new Pawn(true));
                break;
            case "bishop":
                testPiece(new Bishop(true));
                break;
            case "knight":
                testPiece(new Knight(true));
                break;
            case "queen":
                testPiece(new Queen(true));
                break;
            case "king":
                kingAttacked();
                break;
            case "castling":
                castlingTest();
                break;
            case "conversion":
                testConversion();
                break;
            default:
                this.resetBoard();
                break;
        }
    }

    public Spot getBox(int x, int y) throws Exception {
        if (x < 0 || x > 7 || y < 0 || y > 7) {
            throw new Exception("Index out of bound");
        }
        return boxes[x][y];
    }

    public void resetBoard() {
        // Initialize white pieces
        boxes[0][0] = new Spot(0, 0, new Rook(true));
        boxes[1][0] = new Spot(1, 0, new Knight(true));
        boxes[2][0] = new Spot(2, 0, new Bishop(true));
        boxes[3][0] = new Spot(3, 0, new Queen(true));
        boxes[4][0] = new Spot(4, 0, new King(true));
        boxes[5][0] = new Spot(5, 0, new Bishop(true));
        boxes[6][0] = new Spot(6, 0, new Knight(true));
        boxes[7][0] = new Spot(7, 0, new Rook(true));

        boxes[0][1] = new Spot(0, 1, new Pawn(true));
        boxes[1][1] = new Spot(1, 1, new Pawn(true));
        boxes[2][1] = new Spot(2, 1, new Pawn(true));
        boxes[3][1] = new Spot(3, 1, new Pawn(true));
        boxes[4][1] = new Spot(4, 1, new Pawn(true));
        boxes[5][1] = new Spot(5, 1, new Pawn(true));
        boxes[6][1] = new Spot(6, 1, new Pawn(true));
        boxes[7][1] = new Spot(7, 1, new Pawn(true));


        // Initialize black pieces
        boxes[0][7] = new Spot(0, 7, new Rook(false));
        boxes[1][7] = new Spot(1, 7, new Knight(false));
        boxes[2][7] = new Spot(2, 7, new Bishop(false));
        boxes[3][7] = new Spot(3, 7, new Queen(false));
        boxes[4][7] = new Spot(4, 7, new King(false));
        boxes[5][7] = new Spot(5, 7, new Bishop(false));
        boxes[6][7] = new Spot(6, 7, new Knight(false));
        boxes[7][7] = new Spot(7, 7, new Rook(false));

        boxes[0][6] = new Spot(0, 6, new Pawn(false));
        boxes[1][6] = new Spot(1, 6, new Pawn(false));
        boxes[2][6] = new Spot(2, 6, new Pawn(false));
        boxes[3][6] = new Spot(3, 6, new Pawn(false));
        boxes[4][6] = new Spot(4, 6, new Pawn(false));
        boxes[5][6] = new Spot(5, 6, new Pawn(false));
        boxes[6][6] = new Spot(6, 6, new Pawn(false));
        boxes[7][6] = new Spot(7, 6, new Pawn(false));


        // initialize remaining boxes without any piece
        for (int i = 2; i < 6; i++) {
            for (int j = 0; j < 8; j++) {
                boxes[j][i] = new Spot(j, i, null);
            }
        }
    }

    public void testPiece(Piece piece) {
        piece.setWhite(true);
        // initialize remaining boxes without any piece
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                boxes[j][i] = new Spot(j, i, null);
            }
        }
        // set the piece:
        boxes[4][3] = new Spot(4, 3, piece);

        // set pawns:
        boxes[2][1] = new Spot(2, 1, new Pawn(false));
        boxes[3][1] = new Spot(3, 1, new Pawn(false));
        boxes[4][1] = new Spot(4, 1, new Pawn(false));
        boxes[5][1] = new Spot(5, 1, new Pawn(false));
        boxes[6][1] = new Spot(6, 1, new Pawn(false));

        boxes[2][5] = new Spot(2, 5, new Pawn(false));
        boxes[3][5] = new Spot(3, 5, new Pawn(false));
        boxes[4][5] = new Spot(4, 5, new Pawn(false));
        boxes[5][5] = new Spot(5, 5, new Pawn(false));
        boxes[6][5] = new Spot(6, 5, new Pawn(false));

        boxes[2][2] = new Spot(2, 2, new Pawn(false));
        boxes[2][3] = new Spot(2, 3, new Pawn(false));
        boxes[2][4] = new Spot(2, 4, new Pawn(false));

        boxes[6][2] = new Spot(6, 2, new Pawn(false));
        boxes[6][3] = new Spot(6, 3, new Pawn(false));
        boxes[6][4] = new Spot(6, 4, new Pawn(false));
    }

    public void testConversion() {
        // initialize remaining boxes without any piece
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                boxes[j][i] = new Spot(j, i, null);
            }
        }

        boxes[1][1] = new Spot(1, 1, new Pawn(false));
        boxes[6][6] = new Spot(6, 6, new Pawn(true));

    }

    public void kingAttacked() {
        // initialize remaining boxes without any piece
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                boxes[j][i] = new Spot(j, i, null);
            }
        }

        boxes[4][3] = new Spot(4, 3, new King(true));

        boxes[1][2] = new Spot(1, 2, new Bishop(false));
        boxes[1][4] = new Spot(1, 4, new Knight(false));
        boxes[5][1] = new Spot(5, 1, new Queen(false));
        boxes[6][4] = new Spot(6, 4, new Rook(false));

    }

    public void castlingTest() {
        // initialize remaining boxes without any piece
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                boxes[j][i] = new Spot(j, i, null);
            }
        }
        boxes[0][0] = new Spot(0, 0, new Rook(true));
        boxes[4][0] = new Spot(4, 0, new King(true));
        boxes[6][0] = new Spot(6, 0, new Knight(true));
        boxes[7][0] = new Spot(7, 0, new Rook(true));

        boxes[2][4] = new Spot(2, 4, new Queen(false));

        boxes[0][7] = new Spot(0, 7, new Rook(false));
        boxes[4][7] = new Spot(4, 7, new King(false));
        boxes[7][7] = new Spot(7, 7, new Rook(false));

        boxes[0][6] = new Spot(0, 6, new Pawn(false));
        boxes[1][6] = new Spot(1, 6, new Pawn(false));
        boxes[2][6] = new Spot(2, 6, new Pawn(false));

        boxes[6][6] = new Spot(6, 6, new Pawn(true));

        boxes[5][2] = new Spot(5, 2, new Knight(true));

    }
}
