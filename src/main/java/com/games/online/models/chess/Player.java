package com.games.online.models.chess;

import com.games.online.models.User;

public class Player {
    private User user;
    private boolean whiteSide;

    public Player(User user, boolean whiteSide) {
        this.user = user;
        this.whiteSide = whiteSide;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isWhiteSide() {
        return whiteSide;
    }

    public void setWhiteSide(boolean whiteSide) {
        this.whiteSide = whiteSide;
    }

    public String getPlayerUsername(){
        return this.user.getUsername();
    }

    public boolean equals(User user){
        return this.user.getUsername().equals(user.getUsername());
    }
}
