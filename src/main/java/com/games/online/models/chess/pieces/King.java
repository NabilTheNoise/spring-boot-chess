package com.games.online.models.chess.pieces;

import com.games.online.models.chess.Board;
import com.games.online.models.chess.Piece;
import com.games.online.models.chess.Spot;

import java.util.List;

public class King extends Piece {
    private boolean castlingDone = false;
    private boolean moved = false;


    public King(boolean white) {
        super(white);
    }

    public boolean isCastlingDone() {
        return castlingDone;
    }

    public void setCastlingDone(boolean castlingDone) {
        this.castlingDone = castlingDone;
    }

    public boolean isMoved() {
        return moved;
    }

    public void setMoved(boolean moved) {
        this.moved = moved;
    }

    @Override
    public boolean canMove(Board board, Spot start, Spot end) {
        if (end.getPiece() != null && end.getPiece().isWhite() == this.isWhite()) {
            return false;
        }

        int x = Math.abs(start.getX() - end.getX());
        int y = Math.abs(start.getY() - end.getY());

        if (x + y == 1) {
            return !isAttacked(board, end);
        }

        return this.isValidCastling(board, start, end);
    }

    private boolean isValidCastling(Board board, Spot start, Spot end) {
        if (isCastlingDone() || this.isMoved() || isAttacked(board, start)) {
            return false;
        }

        // King side castling
        if (isCastlingMoveKingSide(start, end)) {
            try {
                Piece piece = board.getBox(start.getX() + 3, start.getY()).getPiece();
                if (piece instanceof Rook && !((Rook) piece).isMoved()) {
                    Spot spot = board.getBox(start.getX() + 1, start.getY());
                    if (isAttacked(board, spot) && spot != null) {
                        return false;
                    }

                    spot = board.getBox(start.getX() + 2, start.getY());
                    if (isAttacked(board, spot) && spot != null) {
                        return false;
                    }
                } else {
                    return false;
                }
            } catch (Exception ignored) {
            }
            return true;
        } else if (isCastlingMoveQueenSide(start, end)) {
            try {
                Piece piece = board.getBox(start.getX() - 4, start.getY()).getPiece();
                if (piece instanceof Rook && !((Rook) piece).isMoved()) {
                    Spot spot = board.getBox(start.getX() - 1, start.getY());
                    if (isAttacked(board, spot) && spot != null) {
                        return false;
                    }

                    spot = board.getBox(start.getX() - 2, start.getY());
                    if (isAttacked(board, spot) && spot != null) {
                        return false;
                    }

                    spot = board.getBox(start.getX() - 3, start.getY());
                    if (isAttacked(board, spot) && spot != null) {
                        return false;
                    }
                } else {
                    return false;
                }
            } catch (Exception ignored) {
            }
            return true;
        } else {
            return false;
        }
    }

    public boolean isCastlingMoveKingSide(Spot start, Spot end) {
        return start.getX() + 2 == end.getX();
    }

    public boolean isCastlingMoveQueenSide(Spot start, Spot end) {
        return start.getX() - 2 == end.getX();
    }

    public boolean isCastlingMove(Spot start, Spot end) {
        return isCastlingMoveQueenSide(start, end) || isCastlingMoveKingSide(start, end);
    }

    public boolean isAttacked(Board board, Spot end) {
        // check for knight attacks
        int[] knightSpots = new int[]{-1, 1, -2, 2};
        for (int x : knightSpots) {
            for (int y : knightSpots) {
                try {
                    Piece piece = board.getBox(end.getX() + x, end.getY() + y).getPiece();
                    if (piece instanceof Knight && piece.isWhite() != this.isWhite()) {
                        return true;
                    }
                } catch (Exception ignored) {
                    continue;
                }
            }
        }

        // Check for pawn attacks
        int[] pawnSpots = new int[]{-1, 1};
        for (int x : pawnSpots) {
            for (int y : pawnSpots) {
                try {
                    Piece piece = board.getBox(end.getX() + x, end.getY() + y).getPiece();
                    if (piece instanceof Pawn && piece.isWhite() != this.isWhite()) {
                        // For Black pawn, attack from up
                        if (y > 0 && !piece.isWhite()) {
                            return true;
                        }

                        // For White pawn, attack from down
                        if (y < 0 && piece.isWhite()) {
                            return true;
                        }
                    }
                } catch (Exception ignored) {
                    continue;
                }
            }
        }


        // Rook or Queen attacks
        for (int x = end.getX() + 1; x < 8; x++) {
            try {
                Piece piece = board.getBox(x, end.getY()).getPiece();
                if ((piece instanceof Rook || piece instanceof Queen) && piece.isWhite() != this.isWhite()) {
                    return true;
                }

                if (piece != null && piece.isWhite() == this.isWhite()) {
                    break;
                }
            } catch (Exception ignored) {
                break;
            }
        }

        for (int x = end.getX() - 1; x >= 0; x--) {
            try {
                Piece piece = board.getBox(x, end.getY()).getPiece();
                if ((piece instanceof Rook || piece instanceof Queen) && piece.isWhite() != this.isWhite()) {
                    return true;
                }

                if (piece != null && piece.isWhite() == this.isWhite()) {
                    break;
                }
            } catch (Exception ignored) {
                break;
            }
        }

        for (int y = end.getY() + 1; y < 8; y++) {
            try {
                Piece piece = board.getBox(end.getX(), y).getPiece();
                if ((piece instanceof Rook || piece instanceof Queen) && piece.isWhite() != this.isWhite()) {
                    return true;
                }

                if (piece != null && piece.isWhite() == this.isWhite()) {
                    break;
                }
            } catch (Exception ignored) {
                break;
            }
        }

        for (int y = end.getY() - 1; y >= 0; y--) {
            try {
                Piece piece = board.getBox(end.getX(), y).getPiece();
                if ((piece instanceof Rook || piece instanceof Queen) && piece.isWhite() != this.isWhite()) {
                    return true;
                }

                if (piece != null && piece.isWhite() == this.isWhite()) {
                    break;
                }
            } catch (Exception ignored) {
                break;
            }
        }


        // Bishop or Queen attacks
        for (int step = 1; step < 8; step++) {
            try {
                Piece piece = board.getBox(end.getX() + step, end.getY() + step).getPiece();
                if ((piece instanceof Bishop || piece instanceof Queen) && piece.isWhite() != this.isWhite()) {
                    return true;
                }

                if (piece != null && piece.isWhite() == this.isWhite()) {
                    break;
                }
            } catch (Exception ignored) {
                break;
            }
        }

        for (int step = 1; step < 8; step++) {
            try {
                Piece piece = board.getBox(end.getX() - step, end.getY() - step).getPiece();
                if ((piece instanceof Bishop || piece instanceof Queen) && piece.isWhite() != this.isWhite()) {
                    return true;
                }

                if (piece != null && piece.isWhite() == this.isWhite()) {
                    break;
                }
            } catch (Exception ignored) {
                break;
            }
        }

        for (int step = 1; step < 8; step++) {
            try {
                Piece piece = board.getBox(end.getX() + step, end.getY() - step).getPiece();
                if ((piece instanceof Bishop || piece instanceof Queen) && piece.isWhite() != this.isWhite()) {
                    return true;
                }

                if (piece != null && piece.isWhite() == this.isWhite()) {
                    break;
                }
            } catch (Exception ignored) {
                break;
            }
        }

        for (int step = 1; step < 8; step++) {
            try {
                Piece piece = board.getBox(end.getX() - step, end.getY() + step).getPiece();
                if ((piece instanceof Bishop || piece instanceof Queen) && piece.isWhite() != this.isWhite()) {
                    return true;
                }

                if (piece != null && piece.isWhite() == this.isWhite()) {
                    break;
                }
            } catch (Exception ignored) {
                break;
            }
        }

        return false;
    }

    public boolean hasMoves(Board board, Spot start) {
        int[] spots = new int[]{-1, 0, 1};
        for (int x : spots) {
            for (int y : spots) {
                if (x == 0 && y == 0) {
                    continue;
                }
                try {
                    Spot spot = board.getBox(start.getX() + x, start.getY() + y);
                    Piece piece = spot.getPiece();
                    if (piece == null) {
                        if (!this.isAttacked(board, spot)) {
                            return true;
                        }
                    }
                    if (piece != null && piece.isWhite() != this.isWhite()) {
                        if (!this.isAttacked(board, spot)) {
                            return true;
                        }
                    }
                } catch (Exception ignored) {
                }
            }
        }
        return false;
    }

    @Override
    public String getName() {
        return String.format("King(%s)", isWhite() ? "White" : "Black");
    }
}
