package com.games.online.models.chess;


public class GameStatus {
    private EGameStatus name;

    public GameStatus(EGameStatus name) {
        this.name = name;
    }

    public EGameStatus getName() {
        return name;
    }

    public void setName(EGameStatus name) {
        this.name = name;
    }
}
