package com.games.online.models.chess.pieces;

import com.games.online.models.chess.Board;
import com.games.online.models.chess.Piece;
import com.games.online.models.chess.Spot;

public class Bishop extends Piece {
    public Bishop(boolean white) {
        super(white);
    }

    @Override
    public boolean canMove(Board board, Spot start, Spot end) throws Exception {
        if (end.getPiece() != null && end.getPiece().isWhite() == this.isWhite()) {
            return false;
        }

        int x = Math.abs(start.getX() - end.getX());
        int y = Math.abs(start.getY() - end.getY());

        if (x == y && x > 0) {
            int xDownstream = start.getX();
            int yDownstream = start.getY();
            int xStep;
            int yStep;

            if (start.getX() < end.getX()) {
                xStep = 1;
            } else {
                xStep = -1;
            }
            if (start.getY() < end.getY()) {
                yStep = 1;
            } else {
                yStep = -1;
            }

            for (int i = 0; i < x - 1; i++) {
                xDownstream += xStep;
                yDownstream += yStep;
                if (board.getBox(xDownstream, yDownstream).getPiece() != null) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public String getName() {
        return String.format("Bishop(%s)", isWhite() ? "White" : "Black");
    }
}
