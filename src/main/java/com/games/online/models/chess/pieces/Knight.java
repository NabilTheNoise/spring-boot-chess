package com.games.online.models.chess.pieces;

import com.games.online.models.chess.Board;
import com.games.online.models.chess.Piece;
import com.games.online.models.chess.Spot;

public class Knight extends Piece {
    public Knight(boolean white) {
        super(white);
    }

    @Override
    public boolean canMove(Board board, Spot start, Spot end) {
        if (end.getPiece() != null && end.getPiece().isWhite() == this.isWhite()) {
            return false;
        }

        int x = Math.abs(start.getX() - end.getX());
        int y = Math.abs(start.getY() - end.getY());

        return x * y == 2;
    }

    @Override
    public String getName() {
        return String.format("Knight(%s)", isWhite()?"White":"Black");
    }
}
