package com.games.online.models.chess.pieces;

import com.games.online.models.chess.Board;
import com.games.online.models.chess.Piece;
import com.games.online.models.chess.Spot;

public class Rook extends Piece {
    private boolean moved = false;

    public Rook(boolean white) {
        super(white);
    }

    public boolean isMoved() {
        return moved;
    }

    public void setMoved(boolean moved) {
        this.moved = moved;
    }

    @Override
    public boolean canMove(Board board, Spot start, Spot end) throws Exception {
        if (end.getPiece() != null && end.getPiece().isWhite() == this.isWhite()) {
            return false;
        }

        int x = Math.abs(start.getX() - end.getX());
        int y = Math.abs(start.getY() - end.getY());

        if (x > 0 && y == 0) {
            int upstream = start.getX();
            int downstream = end.getX();
            if (start.getX() < end.getX()) {
                upstream = end.getX();
                downstream = start.getX();
            }

            for (int i = downstream + 1; i < upstream; i++) {
                if (board.getBox(i, start.getY()).getPiece() != null) {
                    return false;
                }
            }
            return true;
        } else if (y > 0 && x == 0) {
            int upstream = start.getY();
            int downstream = end.getY();
            if (start.getY() < end.getY()) {
                upstream = end.getY();
                downstream = start.getY();
            }

            for (int i = downstream + 1; i < upstream; i++) {
                if (board.getBox(start.getX(), i).getPiece() != null) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String getName() {
        return String.format("Rook(%s)", isWhite() ? "White" : "Black");
    }
}
