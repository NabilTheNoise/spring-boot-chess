package com.games.online.models.chess;

import com.games.online.models.chess.pieces.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "games")
public class Game {

    @Id
    private String id;
    private Player[] players;
    private Player currentTurn;
    private Board board;
    private List<Move> movesPlayed;
    private GameStatus gameStatus;

    public Game() {
    }

    public Game(Player player) {
        this.players = new Player[2];
        this.board = new Board();
        this.movesPlayed = new ArrayList<>();
        this.gameStatus = new GameStatus(EGameStatus.ACTIVE);

        players[0] = player;
        players[1] = null;
        if (player.isWhiteSide()) {
            currentTurn = player;
        } else {
            currentTurn = null;
        }
    }

    public Game(Player player, String type) {
        this.players = new Player[2];
        this.board = new Board(type);
        this.movesPlayed = new ArrayList<>();
        this.gameStatus = new GameStatus(EGameStatus.ACTIVE);

        player.setWhiteSide(true);
        players[0] = player;
        currentTurn = player;
        Player player2 = new Player(player.getUser(), false);
        players[1] = player2;

    }

    public boolean isEnd() {
        return this.gameStatus.getName() != EGameStatus.ACTIVE;
    }

    public boolean playerMove(Player player, int startX, int startY,
                              int endX, int endY, String convertTo) throws Exception {
        Spot startBox = this.board.getBox(startX, startY);
        Spot endBox = this.board.getBox(endX, endY);
        Move move = new Move(player, startBox, endBox);
        return this.makeMove(player, move, convertTo);
    }

    private boolean makeMove(Player player, Move move, String convertTo) throws Exception {

        Piece sourcePiece = move.getStart().getPiece();
        if (sourcePiece == null) {
            return false;
        }

        // Check if it's this player turn to make a move
        if (player != currentTurn) {
            return false;
        }

        // Player should only make moves on it's own pieces
        if (sourcePiece.isWhite() != player.isWhiteSide()) {
            return false;
        }

        // Valid move?
        if (!sourcePiece.canMove(board, move.getStart(), move.getEnd())) {
            return false;
        }

        // Kill?
        Piece destPiece = move.getEnd().getPiece();
        if (destPiece != null) {
            destPiece.setKilled(true);
            move.setPieceKilled(destPiece);
        }

        // Castling?
        if (sourcePiece instanceof King
                && ((King) sourcePiece).isCastlingMove(move.getStart(), move.getEnd())) {
            move.setCastlingMove(true);
            if (((King) sourcePiece).isCastlingMoveKingSide(move.getStart(), move.getEnd())) {
                Spot rookNewSpot = board.getBox(move.getEnd().getX() - 1, move.getEnd().getY());
                Spot rookOldSpot = board.getBox(move.getEnd().getX() + 1, move.getEnd().getY());

                ((Rook) rookOldSpot.getPiece()).setMoved(true);

                rookNewSpot.setPiece(rookOldSpot.getPiece());
                rookOldSpot.setPiece(null);
            }

            if (((King) sourcePiece).isCastlingMoveQueenSide(move.getStart(), move.getEnd())) {
                Spot rookNewSpot = board.getBox(move.getEnd().getX() + 1, move.getEnd().getY());
                Spot rookOldSpot = board.getBox(move.getEnd().getX() - 2, move.getEnd().getY());

                ((Rook) rookOldSpot.getPiece()).setMoved(true);

                rookNewSpot.setPiece(rookOldSpot.getPiece());
                rookOldSpot.setPiece(null);
            }
        }


        // Move piece from the stat box to end box
        move.getEnd().setPiece(move.getStart().getPiece());
        move.getStart().setPiece(null);


        // Pawn Conversion?
        if (sourcePiece instanceof Pawn && ((Pawn) sourcePiece).isAtEnd(move.getEnd())) {
            Piece conversion;
            switch (convertTo) {
                case "knight":
                    conversion = new Knight(sourcePiece.isWhite());
                    break;
                case "rook":
                    conversion = new Rook(sourcePiece.isWhite());
                    break;
                case "bishop":
                    conversion = new Bishop(sourcePiece.isWhite());
                    break;
                case "queen":
                    conversion = new Queen(sourcePiece.isWhite());
                    break;
                default:
                    return false;
            }
            move.getEnd().setPiece(conversion);
        }


        // After the move is done, is the king still under attack?
        boolean checked = false;
        if (sourcePiece instanceof King) {
            checked = true;
        }
        for (int x = 0; x < 8; x++) {
            if (checked) {
                break;
            }
            for (int y = 0; y < 8; y++) {
                if (checked) {
                    break;
                }
                Spot spot = board.getBox(x, y);
                Piece piece = spot.getPiece();
                if (piece instanceof King) {
                    if (piece.isWhite() == currentTurn.isWhiteSide()) {
                        checked = true;
                        if (((King) piece).isAttacked(board, spot)) {
                            // Undo the move
                            move.getStart().setPiece(sourcePiece);
                            if (destPiece != null) {
                                move.getEnd().setPiece(destPiece);
                                destPiece.setKilled(false);
                                move.setPieceKilled(null);
                            }
                            if (destPiece == null) {
                                move.getEnd().setPiece(null);
                            }
                            if (!((King) piece).hasMoves(board, spot)) {
                                if (player.isWhiteSide()) {
                                    this.setGameStatus(new GameStatus(EGameStatus.BLACK_WIN));
                                } else {
                                    this.setGameStatus(new GameStatus(EGameStatus.WHITE_WIN));
                                }
                            }
                            return false;
                        } else {
                            break;
                        }
                    }
                }
            }
        }

        // Store the move
        movesPlayed.add(move);


        // king is moved?
        if (sourcePiece instanceof King && !((King) sourcePiece).isMoved()) {
            ((King) sourcePiece).setMoved(true);
        }

        // rook is moved?
        if (sourcePiece instanceof Rook && !((Rook) sourcePiece).isMoved()) {
            ((Rook) sourcePiece).setMoved(true);
        }

        // Is it a win?
        if (destPiece instanceof King) {
            if (player.isWhiteSide()) {
                this.setGameStatus(new GameStatus(EGameStatus.WHITE_WIN));
            } else {
                this.setGameStatus(new GameStatus(EGameStatus.BLACK_WIN));
            }
        }

        // Set the current turn to the other player
        if (this.currentTurn.getPlayerUsername().equals(players[0].getPlayerUsername())) {
            this.currentTurn = players[1];
        } else {
            this.currentTurn = players[0];
        }

        return true;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Player[] getPlayers() {
        return players;
    }

    public void setPlayers(Player[] players) {
        this.players = players;
    }

    public void setSecondPlayer(Player player) {
        this.players[1] = player;
    }

    public Player getCurrentTurn() {
        return currentTurn;
    }

    public void setCurrentTurn(Player currentTurn) {
        this.currentTurn = currentTurn;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    public List<Move> getMovesPlayed() {
        return movesPlayed;
    }

    public void setMovesPlayed(List<Move> movesPlayed) {
        this.movesPlayed = movesPlayed;
    }
}
