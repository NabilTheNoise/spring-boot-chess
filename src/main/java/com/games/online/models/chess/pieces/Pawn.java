package com.games.online.models.chess.pieces;

import com.games.online.models.chess.Board;
import com.games.online.models.chess.Piece;
import com.games.online.models.chess.Spot;

public class Pawn extends Piece {
    public Pawn(boolean white) {
        super(white);
    }

    @Override
    public boolean canMove(Board board, Spot start, Spot end) throws Exception {
        int x = Math.abs(start.getX() - end.getX());
        int y = Math.abs(start.getY() - end.getY());
        int direction = end.getY() - start.getY();

        // Check pawn direction
        if ((direction < 0 && this.isWhite()) || (direction > 0 && !this.isWhite())) {
            return false;
        }

        // Moving 2 spots at the beginning?
        if (direction < 0 && y == 2 && x == 0 && start.getY() == 6) {
            if (board.getBox(end.getX(), end.getY()).getPiece() != null) {
                return false;
            }
            return board.getBox(end.getX(), end.getY() + 1).getPiece() == null;
        }

        if (direction > 0 && y == 2 && x == 0 && start.getY() == 1) {
            if (board.getBox(end.getX(), end.getY()).getPiece() != null) {
                return false;
            }
            return board.getBox(end.getX(), end.getY() - 1).getPiece() == null;
        }

        if (y == 1) {
            Piece destPiece = board.getBox(end.getX(), end.getY()).getPiece();

            // Is Attacking?
            if (x == 1) {
                if (destPiece == null) {
                    return false;
                }
                return destPiece.isWhite() != this.isWhite() && !(destPiece instanceof King);

            } else if (x == 0) {
                return destPiece == null;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean isAtEnd(Spot end) {
        return end.getY() == 0 || end.getY() == 7;
    }

    @Override
    public String getName() {
        return String.format("Pawn(%s)", isWhite() ? "White" : "Black");
    }
}
