package com.games.online.models.chess.pieces;

import com.games.online.models.chess.Board;
import com.games.online.models.chess.Piece;
import com.games.online.models.chess.Spot;

public class Queen extends Piece {
    public Queen(boolean white) {
        super(white);
    }

    @Override
    public boolean canMove(Board board, Spot start, Spot end) throws Exception {
        if (end.getPiece() != null && end.getPiece().isWhite() == this.isWhite()) {
            return false;
        }

        int x = Math.abs(start.getX() - end.getX());
        int y = Math.abs(start.getY() - end.getY());

        int xDownstream = start.getX();
        int yDownstream = start.getY();
        int xStep;
        int yStep;

        if (x == y && x > 0) {
            if (start.getX() < end.getX()) {
                xStep = 1;
            } else {
                xStep = -1;
            }
            if (start.getY() < end.getY()) {
                yStep = 1;
            } else {
                yStep = -1;
            }

            for (int i = 0; i < x - 1; i++) {
                xDownstream += xStep;
                yDownstream += yStep;

                if (board.getBox(xDownstream, yDownstream).getPiece() != null) {
                    return false;
                }

            }
            return true;

        } else if (x > 0 && y == 0) {
            int upstream = start.getX();
            int downstream = end.getX();
            if (start.getX() < end.getX()) {
                upstream = end.getX();
                downstream = start.getX();
            }

            for (int i = downstream + 1; i < upstream; i++) {
                if (board.getBox(i, start.getY()).getPiece() != null) {
                    return false;
                }
            }
            return true;
        } else if (y > 0 && x == 0) {
            int upstream = start.getY();
            int downstream = end.getY();
            if (start.getY() < end.getY()) {
                upstream = end.getY();
                downstream = start.getY();
            }

            for (int i = downstream + 1; i < upstream; i++) {
                if (board.getBox(start.getX(), i).getPiece() != null) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String getName() {
        return String.format("Queen(%s)", isWhite() ? "White" : "Black");
    }
}
