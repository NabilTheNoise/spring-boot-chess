package com.games.online.models;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_MODERATOR
}
