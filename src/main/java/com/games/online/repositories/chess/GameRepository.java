package com.games.online.repositories.chess;

import com.games.online.models.chess.Game;
import com.games.online.models.chess.Player;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GameRepository extends MongoRepository<Game, String> {
    Optional<Game> findById(String id);

    void deleteById(String id);

    boolean existsById(String id);

    List<Game> findAllByPlayers(Player player);
}
